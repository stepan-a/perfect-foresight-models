function [ys, check] = rbc5_steadystate(ys, exe)
    global M_
    check = 0;
    beta = M_.params(1); alpha = M_.params(2); delta = M_.params(3); rho = M_.params(4);
    LoggedProductivity = 0;
    Capital = (alpha/(1/beta-1+delta))^(1/(1-alpha));
    Consumption = Capital^alpha-delta*Capital;
    ys = [Consumption; Capital; LoggedProductivity];