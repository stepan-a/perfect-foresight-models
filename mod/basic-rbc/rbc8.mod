// Permanent shock on productivity -> Simulation of the transition induced by a shock in period one.
@#include "rbc.mod"

steady_state_model;
  LoggedProductivity = LoggedProductivityInnovation/(1-rho);
  Capital = (exp(LoggedProductivity)*alpha/(1/beta-1+delta))^(1/(1-alpha));
  Consumption = exp(LoggedProductivity)*Capital^alpha-delta*Capital;
end;

set_time(1Q1);

initval;
  LoggedProductivityInnovation = 0;
end;

steady;

endval;
  LoggedProductivityInnovation = 0;
end;

steady;

shocks;
var LoggedProductivityInnovation;
periods 1;
values .1;
end;

simul(periods=200);

plot(Simulated_time_series.Capital(1Q1:25Q4));
