@#include "rbc.mod"

steady_state_model;
  LoggedProductivity = 0;
  Capital = (alpha/(1/beta-1+delta))^(1/(1-alpha));
  Consumption = Capital^alpha-delta*Capital;
end;

resid;
