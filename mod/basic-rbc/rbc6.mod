// Simulation of a transition to the steady state if there is a gap between the initial condition and the steady state.
@#include "rbc.mod"

steady_state_model;
  LoggedProductivity = LoggedProductivityInnovation/(1-rho);
  Capital = (exp(LoggedProductivity)*alpha/(1/beta-1+delta))^(1/(1-alpha));
  Consumption = exp(LoggedProductivity)*Capital^alpha-delta*Capital;
end;

set_time(1Q1);

initval;
  LoggedProductivityInnovation = 0;
  LoggedProductivity = .05;
  Capital = 17.5;
end;

endval;
  LoggedProductivityInnovation = 0;
end;

steady;

simul(periods=200);

plot(Simulated_time_series.Capital(1Q1:25Q4));
