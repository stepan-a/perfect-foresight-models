ROOT_PATH = .

slides:
	$(MAKE) -C $(ROOT_PATH)/tex all

push:
	$(MAKE) -C $(ROOT_PATH)/tex push

all: slides
