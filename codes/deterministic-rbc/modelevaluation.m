function [Residuals,Jacobian] = modelevaluation(Y, parameters, initialstate, terminalcontrol)
% stephane.adjemian@univ-lemans.fr (2014)

T = length(Y)/2; % The number of periods, T, should be an integer.
Residuals = zeros(length(Y),1);

if nargout>1
    number_of_nonzero_elements = 10+(T-2)*6;
    Jacobian = sparse([],[],[],length(Y),length(Y),number_of_nonzero_elements);
end

% Period 0
y = [initialstate; Y(1:3)];
if nargout<2
    Residuals(1) = euler(y,parameters);
    Residuals(2) = transition(y,parameters);
else
    [Residuals(1), JEuler] = euler(y,parameters);
    [Residuals(2), JTransition] = transition(y,parameters);
    Jacobian(1,1) = JEuler(2);
    Jacobian(1,2) = JEuler(3);
    Jacobian(1,3) = JEuler(4);
    Jacobian(2,1) = JTransition(2);
    Jacobian(2,2) = JTransition(3);
end
yoffset = 1;
roffset = 2;

% Periods 1 to T-2
for t=1:T-2
    y = Y(yoffset+(1:4));
    if nargout<2
        Residuals(roffset+1) = euler(y, parameters);
        Residuals(roffset+2) = transition(y, parameters);
    else
        [Residuals(roffset+1), JEuler] = euler(y, parameters);
        [Residuals(roffset+2), Jtransition] = transition(y, parameters);
        Jacobian(roffset+1,yoffset+2) = JEuler(2);
        Jacobian(roffset+1,yoffset+3) = JEuler(3);
        Jacobian(roffset+1,yoffset+4) = JEuler(4);
        Jacobian(roffset+2,yoffset+1) = JTransition(1);
        Jacobian(roffset+2,yoffset+2) = JTransition(2);
        Jacobian(roffset+2,yoffset+3) = JTransition(3);
    end
    roffset = roffset+2;
    yoffset = yoffset+2;
end

% Periods T-1
y = [Y(yoffset+(1:3)); terminalcontrol];
if nargout<2
    Residuals(roffset+1) = euler(y, parameters);
    Residuals(roffset+2) = transition(y, parameters);
else
    [Residuals(roffset+1), JEuler] = euler(y, parameters);
    [Residuals(roffset+2), Jtransition] = transition(y, parameters);
    Jacobian(roffset+1,yoffset+2) = JEuler(2);
    Jacobian(roffset+1,yoffset+3) = JEuler(3);
    Jacobian(roffset+2,yoffset+1) = JTransition(1);
    Jacobian(roffset+2,yoffset+2) = JTransition(2);
    Jacobian(roffset+2,yoffset+3) = JTransition(3);
end