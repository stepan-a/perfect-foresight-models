function [k, c] = modelsimulation(T, kinit, parameters)
% stephane.adjemian@univ-lemans.fr (2014)

% Compute the steady state of the model.
y = modelsteadystate(parameters);
kstar = y(1);
cstar = y(2);

% Create paths for the endogenous variables (this is a very rought initial guess).
Y = [cstar; repmat(y, T-1, 1); kstar];

% Solve for the paths.
noconvergence = 1; maxiter = 100; iter = 1;

warning('off','all')

while noconvergence
    % Evaluate residuals and jacobian
    [F, J] = modelevaluation(Y, parameters, kinit, cstar);
    % Newton iteration
    Y = Y - J\F;
    % Test for convergence
    if max(abs(F))<1e-5
        noconvergence = 0;
        disp('=> Convergence achieved!')
        k = [kinit; Y(1+(1:2:2*T))];
        c = [Y(1:2:2*T); cstar];
    else
        disp(['Iteration number ' int2str(iter) ', max. abs. residual is ' num2str(max(abs(F)))])
        iter = iter+1;
    end
    % Stop if the number of iterations gets to large
    if iter>=maxiter
        disp(['Did not converge after ' int2str(maxit) ' iterations!'])
        k = []; c = [];
        return
    end
    % Stop if the residual gets too large or NaN
    if isnan(max(abs(F))) || max(abs(F))>1e100
        disp('Newton algorithm diverged!')
        k = []; c = [];
        return
    end
end

warning('on','all')