function y = modelsteadystate(parameters)
% stephane.adjemian@univ-lemans.fr (2014)

alpha = parameters(1);
beta = parameters(2);
delta = parameters(3);

kstar = (alpha/((1/beta-1)+delta))^(1/(1-alpha));

y = [ kstar; kstar^alpha - delta*kstar];